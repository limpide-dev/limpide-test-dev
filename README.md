# Limpide test dev

## Installation

1. Cloner le repo
2. Créer une base de données et lancer l'installation de Wordpress
3. Travailler dans le dossier du thème **limpide**

## Exercice

- Créer un custom post type "recette" pouvant être catégorisé ainsi qu'étiqueté.
- Prévoir une page de listing des recettes avec pagination.
- Prévoir également une page de listing des recettes par catégorie et étiquette.
- Sur la page de recette, prévoir un système d'onglet pour switcher entre les ingrédients et le contenu de la recette.

> Les recettes doivent avoir :  
    - une image,  
    - un titre,  
    - une liste d'ingrédient,  
    - une durée de réalisation  
    - un contenu libre (Gutenberg)

> Sur la page de la recette, prévoir un système d'onglet pour switcher entre la liste des ingrédients et le contenu de la recette.

## Point d'attention

Nous ne jugeront que la qualité de votre code (html, css, js, php). Il n'est donc pas nécessaire de faire l'exercice en entier si vous bloquez sur un point. Essayez juste de nous fournir assez d'éléments dans les différents langages cités au début de ce paragraphe.  

Évitez d'utiliser des plugins et des libraires css/js. Nous souhaitons voir votre code, pas celui d'un tier.  

Le plugin ACF pro est mis à disposition si vous en avez besoin.  

## Rendu

- Dump SQL
- Login / Mdp de l'administration
- Lien vers un repository git publique ou zip contenant à minima les dossiers "wp-content/themes/limpide", "wp-content/uploads", "wp-content/plugins"
- Tout autre élément qui vous semblerait pertinent de nous partager
